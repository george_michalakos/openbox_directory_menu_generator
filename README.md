# Openbox Directory Menu Generator
Take a look at the [install guide](#build) or the [manual](#manual).

Command for this menu : `obdirmenu ~ -h -d 3 -o 10000 -s Home -m 'File\ Manager;Terminal\ Emulator;'`

![demo](/demo.png)

Command for this menu : `obdirmenu ~/.bin -f -b -s '~/.bin' -m 'Open;Open\ In\ Terminal;'`

![demo1](/demo1.png)

Command for this menu : `obdirmenu /usr/bin -f -b -g -s Run -m 'bash -c;lxterminal -e;lxterminal -e sudo;'`

![demo2](/demo2.png)

## Build
First **clone the repository**: `user@hostname:~$ git clone https://gitlab.com/george_michalakos/openbox_directory_menu_generator.git`

Next execute the **build command** (You can replace the ' ~/bin/ ' with whatever directory you want): `user@hostname:~$ gcc --std=c89 openbox_directory_menu_generator/obdirmenu.c -o ~/bin/obdirmenu`

Optionaly **remove the folder** with: `user@hostname:~$ rm -rf openbox_directory_menu_generator`


## Installation
First create an openbox pipe menu entry, inside of your desired openbox menu.

Example of a menu.xml.
```xml
<menu id="root-menu" label="Root">
    <separator label="Root"/>
    <menu id="run-menu" label="Run Menu" execute="obdirmenu /usr/bin -f -b -g -s Run -m 'Open;Open\ In\ Terminal;'"/>
    <menu id="directory-menu" label="Directory Menu" execute="obdirmenu ~ -h -d 3 -o 10000 -s Home -m 'Cut;Copy;Paste;Delete;Open;Open\ In\ Terminal;'"/>
</menu>
```
Next copy the contents of the obdirscripts folder to your ~/bin directory. (If you use your own scripts, this step is optional.)

Lastly you need to add the line `export PATH=$PATH:$HOME/bin` to your ~/.config/openbox/environment file.


## Manual
**Usage:** `obdirmenu [Options] [Path]`

**Path:** The path to the menus root directory.

**Options:**
- `-b` : Do not show the script menu at the bottom of the directory/file menu.
- `-d depth` : The program will not traverse directories whose depth exeeds the one that was specified with this option.
- `-f` : Show files and directories.
- `-g` : Groups the executables based on their first letter.
- `-h` : Display hidden files/directories.
- `-l` : Changes the location of the script menu, from the bottom of the file and directory menu to the top of the menu.
- `-m script1;script2;...` : Specifies the scripts that are going to be displayed in the script menu.
- `-o offset` : Adds this offset to the id's of the generated menus.
- `-p` : Show the absolute path of every file/directory.
- `-r` : Reverses the alphabetical sorting order.
- `-s label` : Adds a separator at the begining of the root menu, with the specified label.

**Note fot the -m option:**
When you click on the second entry of this menu, under a file or directory named myfile, openbox executes the command << script2 'myfile' >>.
This means that the first argument of script2 is going to be the name of the given file, for example if script2 was 'run in terminal' and myfile was nano the final command would be << 'run in terminal' 'nano' >>.
