#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void genentries(char *menu, const char *path, const char delim)
{
    int i, delimindex;
    if(!menu || !path) return;

    for(i = 0, delimindex = -1; menu[i]; i++)
        if(menu[i] == delim)
        {
            if(delimindex != i-1)
            {
                menu[i] = '\0';
                printf("<item label=\"%s\"><action name=\"Execute\"><command>%s '%s'</command></action></item>",menu+delimindex+1,menu+delimindex+1,path);
                menu[i] = delim;
            }
            delimindex = i;
        }
}



void genmenu(const char reverse, const char files, const char hidden, const char fullpaths, const char bottom, const char group, const char *menu, char *path, unsigned int pathlen, unsigned int depth, unsigned int *id)
{
    DIR *dir;
    struct dirent *entry;
    char  **paths, *npath, prevchar;
    unsigned int npathlen, pathslen, i;

    pathslen = 0;
    if(depth && (dir = opendir(path)))
    {
        while(entry = readdir(dir))
            if((files || entry->d_type == 4) && strcmp(entry->d_name,".") && strcmp(entry->d_name,"..") && (hidden || entry->d_name[0] != '.')) pathslen++;
        closedir(dir);
    }
    paths = (char **) malloc(pathslen * sizeof(char *));

    pathslen = 0;
    if(depth && (dir = opendir(path)))
    {
        while(entry = readdir(dir))
        {
            if((files || entry->d_type == 4) && strcmp(entry->d_name,".") && strcmp(entry->d_name,"..") && (hidden || entry->d_name[0] != '.'))
            {
                npathlen = pathlen + strlen(entry->d_name) + 1;
                npath = (char *) malloc(npathlen + 1);
                strcpy(npath,path);
                npath[pathlen] = '/';
                npath[pathlen+1] = '\0';
                strcat(npath,entry->d_name);

                for(i = pathslen++; i > 0 && (reverse && strcmp(npath,paths[i-1]) > 0 || !reverse && strcmp(npath,paths[i-1]) < 0); i--)
                    paths[i] = paths[i-1];
                paths[i] = npath;
            }
        }
        closedir(dir);
    }

    prevchar = '\0';
    for(i = 0; i < pathslen; i++)
    {
        npath = paths[i];
        npathlen = strlen(npath);
        if(!fullpaths)
        {
            npath = npath + npathlen;
            while(*(npath-1) != '/') npath--;

            if(group && npath[0] != prevchar)
            {
                if(prevchar) printf("</menu>");
                prevchar = npath[0];
                printf("<menu id=\"%d\" label=\"%c\">",(*id)++,prevchar);
            }
        }

        printf("<menu id=\"%u\" label=\"%s\">",(*id)++,npath);
        genmenu(reverse,files,hidden,fullpaths,bottom,group,menu,paths[i],npathlen,depth-1,id);
        printf("</menu>");
    }

    if(!fullpaths && group && pathslen) printf("</menu>");

    if(!bottom || !pathslen)
    {
        if(pathslen) printf("<separator/>");
        genentries((char *)menu,path,';');
    }

    free(path);
    free(paths);
}



int main(int argc, char **argv)
{
    unsigned int i = 0, depth = -1, id = 0, pathlen = 0;
    char reverse = 0, files = 0, hidden = 0, fullpaths = 0, bottom = 0, group = 0, *menu = NULL, *path = NULL, *separator = NULL, *ptr = NULL;

    for(i = 1; i < argc; i++)
        if(argv[i][0] == '-')
            switch(argv[i][1])
            {
                case 'b':
                    bottom = !bottom;
                break;

                case 'd':
                    if(i+1 < argc) depth = strtoul(argv[++i],&ptr,10);
                break;

                case 'f':
                    files = !files;
                break;

                case 'g':
                    group = !group;
                break;

                case 'h':
                    hidden = !hidden;
                break;

                case 'm':
                    if(i+1 < argc) menu = argv[++i];
                break;

                case 'o':
                    if(i+1 < argc) id = strtoul(argv[++i],&ptr,10);
                break;

                case 'p':
                    fullpaths = !fullpaths;
                break;

                case 'r':
                    if(i+1 < argc) reverse = !reverse;
                break;

                case 's':
                    if(i+1 < argc) separator = argv[++i];
                break;
            }
        else if(!path) path = argv[i];

    if(!path)
    {
        printf("Usage: obdirmenu [Options] [Path]\n\n");
        printf("Path: The path to the menus root directory.\n\n");
        printf("Options:\n  -b : Do not show the script menu at the bottom of the directory/file menu.\n  -d depth : The program will not traverse directories whose depth exeeds the one that was specified with this option.\n  -f : Show files and directories\n  -g : Groups the executables based on their first letter.\n  -h : Display hidden files/directories.\n  -m script1;script2;... : Specifies the scripts that are going to be displayed in the script menu.\n  -o offset : Adds this offset to the id's of the generated menus.\n  -p : Show the absolute path of every file/directory.\n  -r : Reverses the alphabetical sorting order of the directory/file menu.\n  -s label : Adds a separator at the begining of the root menu, with the specified label.\n\n");
        printf("Note fot the -m option: When you click on the second entry of this menu, under a file or directory named myfile, openbox executes the command << 'script2' 'myfile' >>. This means that the first argument of script2 is going to be the name of the given file, for example if script2 was 'run in terminal' and myfile was 'nano' the final command would be << 'run in terminal' 'nano' >>.\n");
        return 1;
    }
    pathlen = strlen(path);

    printf("<openbox_pipe_menu>");
    if(separator) printf("<separator label=\"%s\"/>",separator);
    ptr = malloc(pathlen+1);
    strcpy(ptr,path);
    path = ptr;
    genmenu(reverse,files,hidden,fullpaths,bottom,group,menu,path,pathlen,depth,&id);
    printf("</openbox_pipe_menu>");

    return 0;
}
